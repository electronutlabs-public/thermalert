# thermalert

A Nordic nRF52832 based low resolution thermal camera which detects human presence. It alerts via sound as well as BLE packets.